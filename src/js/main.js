/**
* royalestudios.com
*
* Copyright (c) 2017-07-17 Ricardo Velasco
* Licensed under the GPLv2+ license.
*/

// You can use ECMAScript 2015 modules
// Ex. import module from 'module-name';

(function($) {
  console.log('hello world');
})();