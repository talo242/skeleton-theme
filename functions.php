<?php
/**
 * skeleton-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package skeleton-theme
 */

/**
  * @throws \InvalidArgumentException
  */
	function mix($file)
{
		static $manifest = null;

		if (is_null($manifest)) {
			$url = get_template_directory() . '/dist/mix-manifest.json';
				$manifest = json_decode(file_get_contents($url ), true);
		}

		// die('<pre>'.print_r($manifest, true). '</pre>');

		if (isset($manifest[$file])) {
				return '/'.$manifest[$file];
		}

		trigger_error(sprintf('mix-manifest.json file does not exists', $file), E_USER_ERROR);
}

if ( ! function_exists( 'asociacion_para_todos_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function asociacion_para_todos_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'skeleton-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'asociacion_para_todos_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function asociacion_para_todos_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'asociacion_para_todos_content_width', 640 );
}
add_action( 'after_setup_theme', 'asociacion_para_todos_content_width', 0 );


/**
 * Global javascript variables
 */
$directory = array(
	'home_url' => get_home_url(),
);

/**
 * Enqueue scripts and styles.
 */
function asociacion_para_todos_scripts() {
	wp_enqueue_style( 'skeleton-theme-style',  get_template_directory_uri() . '/dist' . mix('/css/styles.css'));

	wp_register_script( 'skeleton-theme-script',  get_template_directory_uri() . '/dist' . mix('/js/main.js'));
	
	wp_localize_script( 'skeleton-theme-script', 'directory', $directory );
	
	wp_enqueue_script( 'skeleton-theme-script' );

	wp_enqueue_script( 'skeleton-theme-navigation', get_template_directory_uri() . '/dist' . mix('/js/navigation.js'), array(), true );

	wp_enqueue_script( 'skeleton-theme-skip-link-focus-fix', get_template_directory_uri() . '/dist' . mix('/js/skip-link-focus-fix.js'), array(), true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'asociacion_para_todos_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
